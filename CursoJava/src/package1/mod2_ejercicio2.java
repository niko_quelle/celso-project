package package1;

public class mod2_ejercicio2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		byte 		bmin = -128;
		byte 		bmax = 127;
//		reemplazar el 0 por el valor que corresponda en todos los caso
		short 	smin = -32768;
		short 	smax = 32767;
		int 		imax = 2147483647;
		int 		imin =-2147483648;
		long 		lmin =-9223372036854775808l;
		long 		lmax =9223372036854775807l;
		
		System.out.println("tipo\tminimo\tmaximo");
		System.out.println("....\t......\t......");
		System.out.println("\nbyte\t" +bmin + "\t" +bmax);
		System.out.println("\nshort\t" + smin + "\t" +smax);
		System.out.println("\nint\t" + imin + "\t" + imax);
		System.out.println("\nlong\t" + lmin + "\t" + lmax);
		


	}

}
